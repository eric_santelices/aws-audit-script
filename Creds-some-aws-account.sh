#!/usr/bin/env bash
# Set proper controls for AWS CLI access to AutoData Ltd
# proper use % source ./Creds-some-aws-account.sh
#
export AWS_ACCESS_KEY_ID=""
export AWS_SECRET_ACCESS_KEY=""
export AWS_SESSION_TOKEN=""

echo $AWS_ACCESS_KEY_ID
echo $AWS_SECRET_ACCESS_KEY
echo $AWS_SESSION_TOKEN
