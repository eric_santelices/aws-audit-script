# Project AWS Audit Script
## Eric L. Santelices
---
This script assumes you have AWS CLI tools installed, and it assumes that you have AWS API Key's exported.

A simple bash script to:

1. List all users

2. List all groups

3. List all users in groups

It generates a text file that can be uploaded to Impero

Create a file with the export variables called Creds-some-aws-account.sh then run the following


`source ./Creds-some-aws-accout.sh
./aws-Users_group-audit.sh`
