##
# This script assumes you have AWS CLI tools installed, and it assumes that you have AWS API Key's exported.
# A simple bash script to 
# List all users
# List all groups
# List all users in groups
##
# Version: 1.1
# Author: Eric L. Santelices <www.solera.com>
# (C) 20122 Eric L. Santelices, Confidential & Intellectual Property. All rights reserved.
# -------------------------------------------------------

#!/usr/bin/env bash
filedate=$(date '+%d-%m-%Y');
file="aws-audit_$filedate.txt"
echo "Exporting to $file"
#
dt=$(date '+%d/%m/%Y %H:%M:%S');
echo "Current Time: $dt" >> $file
AcctID=$(aws sts get-caller-identity --query "Account" --output text)
echo "Account ID:" $AcctID >> $file
echo "\nListing Users:" >> $file
aws iam list-users --output table --query 'Users[*].UserName' >> $file
echo "\n\n" >> $file
echo "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-\n" >> $file
echo "Checking users who have access to AWS. If a Login Profile cannot be found the user has NO ACCESS" >> $file
loginprofiles=$(aws iam list-users --output text --query 'Users[*].UserName')
arr=($(echo $loginprofiles | tr " " "\n"))
for loginprofile in "${arr[@]}"
    do
        echo  "Listing profile for User: $loginprofile\n"
        aws iam get-login-profile --output table --user-name $loginprofile
        done >> $file 2>> $file
echo "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-\n" >> $file
echo "\n\nListing Groups:" >> $file
aws iam list-groups --output table --query 'Groups[*].GroupName' >> $file
echo "\n\nListing Groups w/memberships:" >> $file
iamgroups=$(aws iam list-groups --output text  --query 'Groups[*].GroupName')
for iamgroup in $iamgroups
    do
        echo "Group $iamgroup listing"
        aws iam get-group --group-name $iamgroup --output table --query 'Users[*].UserName'
    done >> $file
#
echo "\nEnd of User and Group Membership audit file." >> $file
echo "\nEnd of export, please upload $file for audit."